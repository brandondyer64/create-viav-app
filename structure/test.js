const Viav = require('viav.js')
const appRoot = require('app-root-path')
const configSecret = require('./config.secret.json')
const defaultGuildConfig = require('./guildDefault.json')

Viav.start(`${appRoot}/bot.js`, configSecret, defaultGuildConfig, {
  test: true
})
