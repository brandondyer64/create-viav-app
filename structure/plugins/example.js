const { createPlugin } = require('viav.js')

const plugin = createPlugin('Example')
  // An example command
  .command('command', {
    description: 'Test command.',
    run: ({ sendMessage, lang }) => {
      sendMessage(lang.hi)
    }
  })
  // An example discord event
  .clientOn('guildMemberAdd', {
    guild: ([member]) => {
      return member.guild
    },
    run: ([member]) => {
      console.log(`${member.displayName} joined ${member.guild.name}`)
    }
  })
