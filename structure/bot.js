const Viav = require('viav.js')
const client = Viav.client

const configSecret = require('./config.secret.json')
const defaultGuildConfig = require('./guildDefault.json')
const config = require('./config.json')

Viav.setLocales(
  require('./locale/en.json'),
  require('./locale/eo.json'),
  require('./locale/fr.json')
)

Viav.login(config, configSecret, defaultGuildConfig)

Viav.registerPlugins()
