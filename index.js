const { promisify } = require('util')
const fs = require('fs')
const readline = require('readline')
const appRoot = require('app-root-path')

console.log(`${appRoot}`)

const options = {}

// Promisification
const readFile = promisify(fs.readFile)
const writeFile = promisify(fs.writeFile)
const mkdir = promisify(fs.mkdir)
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})
function question(text) {
  return new Promise((resolve, reject) => {
    rl.question(text + ': ', answer => {
      resolve(answer)
    })
  })
}

// Helper functions
async function copy(input, output = null) {
  if (output === null) {
    output = input
  }
  let data = await readFile(`${appRoot}/structure/${input}`, 'utf8')
  for (option in options) {
    data = data
      // "{{{option}}}"
      .split(`"{{{${option}}}}"`)
      .join(options[option])
      // {{option}}
      .split(`{{${option}}}`)
      .join(options[option])
  }
  await writeFile(`./${options.directory}/${output}`, data, 'utf8')
}

async function createDirs() {
  await mkdir(`./${options.directory}`)
  await mkdir(`./${options.directory}/locale`)
  await mkdir(`./${options.directory}/plugins`)
}

async function createFiles() {
  await copy('index.js')
  await copy('test.js')
  await copy('bot.js')
  await copy('package.json')
  await copy('config.json')
  await copy('config.secret.json')
  await copy('guildDefault.json')
  await copy('gitignore', '.gitignore')
  await copy('locale/en.json')
  await copy('locale/eo.json')
  await copy('locale/fr.json')
  await copy('plugins/example.js')
}

// Main
async function start() {
  // Get options
  console.log('\n\n')
  options.name = await question('Name')
  options.prefix = await question('Default Prefix')
  options.token = await question('Bot Token')
  options.clientId = await question('Client ID')
  options.clientSecret = await question('Client Secret')
  console.log('\n\nMySQL Database')
  options.dbHost = await question('DB Host')
  if (options.dbHost) {
    options.useDatabase = true
    options.dbUser = await question('DB User')
    options.dbPass = await question('DB Pass')
    options.dbDB = await question('DB Database')
  } else {
    console.log('Disabling MySQL')
    options.useDatabase = false
    options.dbUser = ''
    options.dbPass = ''
    options.dbDB = ''
  }
  options.directory = options.name
    .toLowerCase()
    .replace(/[^\w\s]/gi, '')
    .replace(/ /g, '-')
  // Create Files
  console.log('Creating Directories')
  await createDirs()
  console.log('Creating Files')
  await createFiles()
  console.log('Done.')
  console.log("Run 'npm i' to install dependencies.")
  console.log("Run 'npm test' to test, and 'npm start' to run production.")
  // Leave
  process.exit()
}
start()
